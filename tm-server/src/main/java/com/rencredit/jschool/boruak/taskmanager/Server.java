package com.rencredit.jschool.boruak.taskmanager;

import com.rencredit.jschool.boruak.taskmanager.bootstrap.Bootstrap;
import com.rencredit.jschool.boruak.taskmanager.config.ServerConfiguration;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.persistence.EntityManager;

public class Server {

    public static void main(String[] args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}
