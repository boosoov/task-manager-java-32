package com.rencredit.jschool.boruak.taskmanager.config;

import com.rencredit.jschool.boruak.taskmanager.service.EntityManagerService;
import com.rencredit.jschool.boruak.taskmanager.service.PropertyService;
import com.rencredit.jschool.boruak.taskmanager.util.EntityManagerFactoryUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@Configuration
@ComponentScan("com.rencredit.jschool.boruak.taskmanager")
public class ServerConfiguration {

    @Bean
    public EntityManagerFactory entityManagerFactory(EntityManagerService entityManagerService) {
        return entityManagerService.getEntityManagerFactory();
    }

    @Bean
    @Scope("prototype")
    public EntityManager entityManager(EntityManagerService entityManagerService) {
        return entityManagerService.getEntityManager();
    }

}
