package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectRepository extends IRepository<ProjectDTO> {

    @NotNull
    List<ProjectDTO> getListDTO();

    @NotNull
    List<Project> getListEntity();

    void addProjectEntity(@NotNull final Project project);

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findAllByUserIdDTO(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserIdEntity(@NotNull final String userId);

    @Nullable
    ProjectDTO findOneDTOById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findOneEntityById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    ProjectDTO findOneDTOByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneEntityByIndex(@NotNull final String userId, @NotNull final Integer index);

    @Nullable
    ProjectDTO findOneDTOByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project findOneEntityByName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    void removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    void removeOneByName(@NotNull String userId, @NotNull String name);

    void clearAll();

}
