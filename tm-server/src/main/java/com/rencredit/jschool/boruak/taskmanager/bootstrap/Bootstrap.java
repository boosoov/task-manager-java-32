package com.rencredit.jschool.boruak.taskmanager.bootstrap;

import com.rencredit.jschool.boruak.taskmanager.api.service.IPropertyService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISessionService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.endpoint.AbstractEndpoint;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.service.PropertyService;
import com.rencredit.jschool.boruak.taskmanager.util.EntityManagerFactoryUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.xml.ws.Endpoint;

@Component
public class Bootstrap {

    @Nullable
    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Autowired
    private IUserService userService;

    @Nullable
    @Autowired
    private ISessionService sessionService;

    public Bootstrap() {
    }

    public void run(@Nullable final String[] args){
        try {
            init();
        } catch (DeniedAccessException e) {
            e.printStackTrace();
        } catch (EmptyUserException e) {
            e.printStackTrace();
        } catch (EmptyLoginException e) {
            e.printStackTrace();
        } catch (EmptyHashLineException e) {
            e.printStackTrace();
        } catch (EmptyPasswordException e) {
            e.printStackTrace();
        } catch (BusyLoginException e) {
            e.printStackTrace();
        } catch (EmptyRoleException e) {
            e.printStackTrace();
        } catch (EmptyElementsException e) {
            e.printStackTrace();
        }
        System.out.println("** SERVER IS RUNNING ** \n");
    }


    public void init() throws DeniedAccessException, EmptyRoleException, EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, EmptyElementsException {
        propertyService.init();
        initUsers();
        sessionService.closeAll();
    }

    private void initUsers() throws DeniedAccessException, EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, EmptyElementsException {
        try {
            userService.addUser("1", "1");
        }catch (Exception e){}
        try {
            userService.addUser("test", "test");
        }catch (Exception e){}
        try {
            userService.addUser("admin", "admin", Role.ADMIN);
        }catch (Exception e){}
    }

    @Autowired
    private void initEndpoint(@NotNull final AbstractEndpoint[] endpoints) {
        for (@NotNull final AbstractEndpoint endpoint : endpoints) {
            registryEndpoint(endpoint);
        }
    }

    private void registryEndpoint(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull String host = propertyService.getServerHost();
        @NotNull Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}
