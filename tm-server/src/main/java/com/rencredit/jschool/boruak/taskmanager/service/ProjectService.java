package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IProjectRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IProjectService;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import com.rencredit.jschool.boruak.taskmanager.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectService extends AbstractService<ProjectDTO> implements IProjectService {

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final ProjectDTO project = new ProjectDTO(userId, name);

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.begin();
            repository.add(project);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @NotNull final ProjectDTO project = new ProjectDTO(userId, name, description);

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.begin();
            repository.add(project);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final ProjectDTO project) throws EmptyProjectException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new EmptyProjectException();
        project.setUserId(userId);

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.begin();
            repository.add(project);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final Project project) throws EmptyProjectException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new EmptyProjectException();

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.begin();
            repository.addProjectEntity(project);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @NotNull final List<ProjectDTO> projects =  repository.findAllByUserIdDTO(userId);
        return projects;
    }

    @Override
    public void clearByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.begin();
            repository.clearByUserId(userId);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws IncorrectIndexException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @Nullable final ProjectDTO project = repository.findOneDTOByIndex(userId, index);
        return project;
    }

    @Nullable
    @Override
    public ProjectDTO findOneByName(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @Nullable final ProjectDTO project = repository.findOneDTOByName(userId, name);
        return project;
    }

    @Nullable
    @Override
    public ProjectDTO findOneDTOById(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @Nullable final ProjectDTO project = repository.findOneDTOById(userId, id);
        return project;
    }

    @Nullable
    @Override
    public Project findOneEntityById(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @Nullable final Project project = repository.findOneEntityById(userId, id);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws EmptyProjectException, EmptyNameException, EmptyIdException, EmptyUserIdException, EmptyDescriptionException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @Nullable final ProjectDTO project = findOneDTOById(userId, id);
        if (project == null) throw new EmptyProjectException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.begin();
            repository.merge(project);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }

        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws EmptyProjectException, EmptyNameException, IncorrectIndexException, EmptyUserIdException, EmptyDescriptionException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new EmptyProjectException();
        project.setName(name);
        project.setDescription(description);

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.begin();
            repository.merge(project);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }

        return project;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final ProjectDTO project) throws EmptyProjectException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new EmptyProjectException();

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.begin();
            repository.remove(project);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws IncorrectIndexException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.begin();
            repository.removeOneByIndex(userId, index);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.begin();
            repository.removeOneByName(userId, name);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.begin();
            repository.removeOneById(userId, id);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> getList() {
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        @NotNull final List<ProjectDTO> projects =  repository.getListDTO();
        return projects;
    }

    @Override
    public void clearAll() {
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.begin();
            repository.clearAll();
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

}
