package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IRepository;
import com.rencredit.jschool.boruak.taskmanager.dto.AbstractEntityDTO;
import com.rencredit.jschool.boruak.taskmanager.service.EntityManagerService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.Collection;

public class AbstractRepository<E extends AbstractEntityDTO> implements IRepository<E> {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    public void begin() {
        entityManager.getTransaction().begin();
    }

    public void commit() {
        entityManager.getTransaction().commit();
    }

    public void rollback() {
        entityManager.getTransaction().rollback();
    }

    public void close() {
        entityManager.close();
    }

    public void add(@NotNull final E record) {
        entityManager.persist(record);
    }

    public void remove(@NotNull final E record) {
        entityManager.remove(record);
    }

    public void clearAll() {
        entityManager.clear();
    }

    public void load(@NotNull final Collection<E> records) {
        clearAll();
        merge(records);
    }

    public void load(@NotNull final E... records) {
        clearAll();
        merge(records);
    }

    public void merge(@NotNull final Collection<E> records) {
        for (@NotNull final E record : records) {
            entityManager.merge(record);
        }
    }

    public void merge(@NotNull final E... records) {
        for (@NotNull final E record : records) {
            entityManager.merge(record);
        }
    }

    public void merge(@NotNull final E record) {
        entityManager.merge(record);
    }

}
