package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ICommandRepository;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractListener> commands = new LinkedHashMap<>();

    public void putCommand(@NotNull final String name, @NotNull final AbstractListener command) {
        commands.put(name, command);
    }

    @NotNull
    @Override
    public String[] getCommands() {
        @NotNull final String[] result = new String[commands.size()];
        int index = 0;
        for (@Nullable Map.Entry<String, AbstractListener> command : commands.entrySet()) {
            @NotNull final StringBuilder resultString = new StringBuilder();
            if (command == null) continue;
            if (!command.getValue().name().isEmpty()) resultString.append(command.getValue().name());
            if (!command.getValue().description().isEmpty())
                resultString.append(": ").append(command.getValue().description());
            result[index] = resultString.toString();
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    @NotNull
    @Override
    public String[] getArgs() {
        @NotNull final String[] result = new String[commands.size()];
        int index = 0;
        for (Map.Entry<String, AbstractListener> command : commands.entrySet()) {
            @NotNull final StringBuilder resultString = new StringBuilder();
            @Nullable final String arg = command.getValue().arg();
            if (arg != null && !arg.isEmpty()) {
                resultString.append(command.getValue().arg());
                if (!command.getValue().description().isEmpty())
                    resultString.append(": ").append(command.getValue().description());
                result[index] = resultString.toString();
                index++;
            }
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    @NotNull
    @Override
    public Map<String, AbstractListener> getTerminalCommands() {
        return commands;
    }

}
