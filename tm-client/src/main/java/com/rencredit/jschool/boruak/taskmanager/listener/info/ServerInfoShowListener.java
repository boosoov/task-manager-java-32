package com.rencredit.jschool.boruak.taskmanager.listener.info;

import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ServerInfoShowListener extends AbstractListener {

    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @NotNull
    @Override
    public String description() {
        return "Show show host and port program.";
    }

    @Override
    @EventListener(condition = "@serverInfoShowListener.name() == #event.command")
    public void handle(final ConsoleEvent event)throws NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptySessionException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        System.out.println("[SERVER INFO]");
        @NotNull final SessionDTO session = systemObjectService.getSession();
        System.out.println(adminEndpoint.getHostPort(session));
        System.out.println("OK");
    }

}
