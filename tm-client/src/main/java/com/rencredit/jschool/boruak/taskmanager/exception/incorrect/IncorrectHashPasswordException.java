package com.rencredit.jschool.boruak.taskmanager.exception.incorrect;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class IncorrectHashPasswordException extends AbstractClientException {

    public IncorrectHashPasswordException() {
        super("Error! Hash password incorrect...");
    }

}
