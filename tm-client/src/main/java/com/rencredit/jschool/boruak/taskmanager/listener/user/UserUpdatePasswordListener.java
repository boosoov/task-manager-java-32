package com.rencredit.jschool.boruak.taskmanager.listener.user;

import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import com.rencredit.jschool.boruak.taskmanager.util.ViewUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class UserUpdatePasswordListener extends AbstractListener {

    @Autowired
    private AuthEndpoint authEndpoint;

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "update-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Update password.";
    }

    @Override
    @EventListener(condition = "@userUpdatePasswordListener.name() == #event.command")
    public void handle(final ConsoleEvent event) throws DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyNewPasswordException_Exception, IncorrectHashPasswordException_Exception, EmptyHashLineException_Exception, EmptyUserException {
        System.out.println("Enter new password");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final SessionDTO session = systemObjectService.getSession();
        @Nullable final String userId = authEndpoint.getUserId(session);
        @Nullable final UserDTO user = userEndpoint.updateUserPasswordById(session, userId, password);
        ViewUtil.showUser(user);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
